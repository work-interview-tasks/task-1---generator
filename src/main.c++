#include <fstream>
#include <iostream>
#include <random>
#include <string>

int main(int argc, char **argv) {
  std::random_device rd;
  std::mt19937 mt(rd());
  std::normal_distribution<double> dist{0, 10e6};

  if (argc != 2) {
    std::cout << "Usage " << argv[0] << " <output file name>" << std::endl;
    return 0;
  }

  std::ofstream out{argv[1]};

  if (!out.is_open()) {
    std::cout << "Failed to open file: " << argv[1] << std::endl;
    return 0;
  }

  // speed up cout and cin
  std::ios_base::sync_with_stdio(false);
  // set scientific double format
  std::cout << std::scientific;

  long long counter = 0;
  while (
      // out.tellp() < 1048576u
      out.tellp() < 1073741824u && out.good()) {
    out << dist(mt) << std::endl;
    counter++;
  }

  if (!out.good()) {
    std::cout << "Not enought space on disk!" << std::endl;
  } else {
    std::cout << "Written numbers count: " << counter << std::endl;
  }

  return 0;
}
